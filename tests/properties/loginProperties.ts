export const testUrls = {
    swaglabsUrl: 'https://www.saucedemo.com/v1',
};

export const titles = {
    swaglabs: 'Swag Labs',
};

export const accountUsernames = {
    usernames : ['standard_user', 'problem_user', 'locked_out_user', 'performance_glitch_user'],
    users : ['standard_user','problem_user','performance_glitch_user'],
    locked_username : 'locked_out_user',
    standard_user : 'standard_user',
    performance_glitch_user : 'performance_glitch_user',
    password : 'secret_sauce'
};
export const wrongCredentials = {
    wrongUsernames : ['123Test','standard_user'],
    wrongPasswords : ['password','234Test'],

    /* This is to showcase the feature of calling the
    property by another property inside the properties file by another object
    */ 
    wrongCredentialsError : 'Username and password do not match any user in this service',
};

export const errorMessage = {
    lockedOut : 'Sorry, this user has been locked out.', 
    '123Test-password' : wrongCredentials.wrongCredentialsError, /*here the property is called*/
    '123Test-234Test' : wrongCredentials.wrongCredentialsError,
    'standard_user-password' : wrongCredentials.wrongCredentialsError,
    'standard_user-234Test' : wrongCredentials.wrongCredentialsError,
};
