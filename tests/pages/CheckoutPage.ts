import { Page, Locator } from '@playwright/test';

export class CheckoutPage {
    private page: Page;

    constructor(page: Page) {
        this.page = page;
    }

//..........................PAGE ELEMENTS.............................
private checkoutButton(): Locator  {
    return this.page.locator("//a[@class='btn_action checkout_button']");
}

private itemsinCart(): Locator {
    return this.page.locator("//div[@class='cart_item']");
}

private priceOfCartProduct(attributeValue : String): Locator  {
    return this.page.locator(`//div[contains(@class, 'inventory_item_name') and text()='${attributeValue}']/ancestor::div[@class='cart_list']//div[@class='inventory_item_price']`);
}

//  Checkout : Information Page
private checkoutDetailsFirstName(): Locator  {
    return this.page.locator("#first-name");
}

private checkoutDetailsLastName(): Locator  {
    return this.page.locator("#last-name");
}

private checkoutDetailsPostCode(): Locator  {
    return this.page.locator("#postal-code");
}

private checkoutContinueButton():Locator {
    return this.page.locator("//input[@value='CONTINUE']"); 
}

// Checkout : Overview Page
private itemTotalPrice():Locator {
    return this.page.locator("//div[@class='summary_subtotal_label']"); 
}

// Checkout : Overview Page
private finishButton():Locator {
    return this.page.locator("//a[@class='btn_action cart_button']"); 
}

// Finish Page
private orderCompleteHeading():Locator {
    return this.page.locator("//h2[@class='complete-header']"); 
}


//...............................ACTIONS ............................
// Checkout
async clickCheckoutButton() {
    await this.checkoutButton().click();
} 

// Get Cart Value
async getCartProductPrice(attributeValue : String) {
    return await this.priceOfCartProduct(attributeValue).textContent();
} 

// Enter Checkout Details
async enterCheckoutDetails(firstName: string , lastName: string , postalCode: string) {
    await this.checkoutDetailsFirstName().fill(firstName);
    await this.checkoutDetailsLastName().fill(lastName);
    await this.checkoutDetailsPostCode().fill(postalCode);
    await this.checkoutContinueButton().click();

} 

// Get Item Total Price
async getitemTotalPrice() {
    return await this.itemTotalPrice().textContent();
} 

async clickFinishButton() {
    await this.finishButton().click();
} 

//Get total number of available items
async getNumberOfItemsInCheckout() {
    return await this.itemsinCart().count(); 
      
}

// Get Item Total Price
async getOrderCompleteConfirmation() {
    return await this.orderCompleteHeading().textContent();
} 

}