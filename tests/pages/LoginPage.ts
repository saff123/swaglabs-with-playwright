import { Page, Locator } from '@playwright/test';
import { testUrls } from '../properties/loginProperties';

export class LoginPage {
    private page: Page;
    private baseUrl: string;

    constructor(page: Page) {
        this.page = page;
        this.baseUrl = testUrls.swaglabsUrl;
    }

//..........................PAGE ELEMENTS.............................
    private usernameInput(): Locator  {
        return this.page.locator('#user-name');
    }

    private passwordInput(): Locator {
        return this.page.locator('#password');
    }

    private loginButton(): Locator  {
        return this.page.locator("#login-button");
    }

    private errorMessageLockedUser(): Locator  {
        return this.page.locator('[data-test="error"]');
    }

    
    private openMenu(): Locator  {
        return this.page.locator("//button[contains(text(),'Open Menu')]");
    }

    private logOutLink(): Locator  {
        return this.page.locator("//a[@id='logout_sidebar_link']");
    }
    

    // Navigate to the login page
    async navigateToLoginPage() {
        await this.page.goto(`${this.baseUrl}/index.html`);
    }

//...............................ACTIONS ............................
    // Login and validates authentication
    async signInSwaglabs(username: string, password: string) {
        await this.navigateToLoginPage();
        await this.usernameInput().fill(username);
        await this.passwordInput().fill(password);
        await this.loginButton().click();

        
        // Handles Alerts
        this.page.on('dialog', async (alert) => {
                await alert.accept(); 
            })

    }

    // Verify error text
    async getErrorText() {
        return await this.errorMessageLockedUser().textContent();
        }       

        // Log Out
    async logOut() {
        await this.openMenu().click();
        await this.logOutLink().click();

} 

}