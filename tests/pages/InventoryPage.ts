import { Page, Locator } from '@playwright/test';

export class InventoryPage {
    private page: Page;

    constructor(page: Page) {
        this.page = page;
    }

//..........................PAGE ELEMENTS.............................
/* Made the dynamic which gives the coder more visibility to select the product with the name instead of Id
*/
private inventoryItemName(attributeValue : String): Locator  {
    return this.page.locator(`//div[contains(@class, 'inventory_item_name') and contains(text(), '${attributeValue}')]`);
}

private priceOfProduct(attributeValue : String): Locator  {
    return this.page.locator(`//div[contains(@class, 'inventory_item_name') and text()='${attributeValue}']/ancestor::div[@class='inventory_item']//div[@class='inventory_item_price']`);
}

private addToCartButtonForaSpecificItem(attributeValue : String): Locator  {
    return this.page.locator(`//div[contains(@class, 'inventory_item_name') and text()='${attributeValue}']/ancestor::div[@class='inventory_item']//button[text()='ADD TO CART']`);
}

private addButton(): Locator  {
    return this.page.locator("//button[@class='btn_primary btn_inventory']");
}

private viewCart(): Locator  {
    return this.page.locator("//*[local-name()='svg' and @data-icon='shopping-cart']/*[local-name()='path']");
}



//...............................ACTIONS ............................


// Verify itemName
async getItemPrice(attributeValue : String) {
    return await this.priceOfProduct(attributeValue).textContent();
}   

// Click ADD TO CART
async clickAddToCartButton(attributeValue : String) {
    await this.addToCartButtonForaSpecificItem(attributeValue).click();
} 

// Get Cart Value
async getCartValue() {
    return await this.viewCart().textContent();
} 

// Click View Cart Button
async clickViewCartButton() {
    await this.viewCart().click();
} 

// Click on Add to Cart buttons
async addItemsToCart(number: number) {
    for (let i = 0; i < number; i++) {
        await this.addButton().nth(i).click();
        await new Promise(resolve => setTimeout(resolve, 1000));
    }
}

}
