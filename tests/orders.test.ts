import { test, expect } from '@playwright/test';
import { LoginPage } from './pages/LoginPage';
import { InventoryPage } from './pages/InventoryPage';
import { CheckoutPage } from './pages/CheckoutPage';
import { accountUsernames, testUrls} from './properties/loginProperties';
import { itemNames} from './properties/inventoryProperties';
import { userDetailsCheckout,orderConfirmationMessage} from './properties/checkoutProperties';


const numberOfItems = 3;
const checkoutYourInformationPage = testUrls.swaglabsUrl + '/checkout-step-one.html';

/* Verify Add to Cart for multiple users - Standard, Problem and Performance Glitch user
*/
for (const username of accountUsernames.users) {
test(`Verify Add To Cart: ${username}`, async ({ page }) => {
  const loginPage = new LoginPage(page); 
  await loginPage.signInSwaglabs(username, accountUsernames.password);
  const inventoryPage = new InventoryPage(page);
  await inventoryPage.addItemsToCart(numberOfItems);
  await inventoryPage.clickViewCartButton();
  await page.waitForLoadState(); // used this for the page to load. Firefox issue
  const checkoutPage = new CheckoutPage(page);  
  const totalItemsInCheckout = await checkoutPage.getNumberOfItemsInCheckout();
  expect(totalItemsInCheckout, `Tried to add ${numberOfItems} items but only ${totalItemsInCheckout} got added`).toBe(numberOfItems);
  });
  }

  /* Verify that the order should not be placed with empty cart */
  test('Verify user is not navigated to "Checkout: Your Information" page', async ({ page }) => {
    const loginPage = new LoginPage(page); 
    await loginPage.signInSwaglabs(accountUsernames.standard_user, accountUsernames.password);
    const inventoryPage = new InventoryPage(page);
    await inventoryPage.clickViewCartButton();
    const checkoutPage = new CheckoutPage(page);  
    await checkoutPage.clickCheckoutButton();
    await expect.soft(page,"User is able to navigate to 'Checkout: Your Information' page with an empty cart").not.toHaveURL(checkoutYourInformationPage)
    });

/* Verify item price is the same across home, cart and the final pages
   Verify that the order can be placed by standard, problem and performance glitch users
*/
for (const user of accountUsernames.users) {
test(`Order item : ${itemNames.backpackName} for user : ${user}`, async ({ page }) => {
  const loginPage = new LoginPage(page); 
  await loginPage.signInSwaglabs(accountUsernames.standard_user, accountUsernames.password);
  const inventoryPage = new InventoryPage(page); 
// getting itemPrice to compare in Checkout page 
  const itemPrice= await inventoryPage.getItemPrice(itemNames.backpackName);

/* provide the item name. Code will handle which "ADD TO CART" button needs to be clicked.
* xpath dynamically handled
*/
  await inventoryPage.clickAddToCartButton(itemNames.backpackName);
  await inventoryPage.clickViewCartButton();
  const checkoutPage = new CheckoutPage(page);  
//Verifying the price matches listed in cart and home page
  const itemPriceInCart= await checkoutPage.getCartProductPrice(itemNames.backpackName);

// substring used as the price in the cart is displayed without "$"
  expect(itemPrice?.substring(1)).toBe(itemPriceInCart);
  await checkoutPage.clickCheckoutButton();
  await checkoutPage.enterCheckoutDetails(userDetailsCheckout.firstName,userDetailsCheckout.LastName,userDetailsCheckout.postCode );
//Verifying the price matches listed in the home page and the final page
  const itemTotalPrice = await checkoutPage.getitemTotalPrice();

// substring used to remove the text part
  expect(itemTotalPrice?.substring(12)).toBe(itemPrice);
  await checkoutPage.clickFinishButton();
  const orderC0nfirmationText = await checkoutPage.getOrderCompleteConfirmation();
  expect(orderC0nfirmationText).toBe(orderConfirmationMessage.confirmation);

});
}