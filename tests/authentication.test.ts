import { test, expect } from '@playwright/test';
import { LoginPage } from './pages/LoginPage';
import { testUrls,accountUsernames,errorMessage, wrongCredentials, titles} from './properties/loginProperties';

const inventoryUrl = testUrls.swaglabsUrl + '/inventory.html';
const loginUrl = testUrls.swaglabsUrl + '/index.html';

/* Verify the login for standard, problem, performance_glitch and locked users
* Running the same test for multiple users using nested loop
*/
for (const username of accountUsernames.usernames) {
test(`Login with username: ${username}`, async ({ page }) => {
  const loginPage = new LoginPage(page); 
  await loginPage.signInSwaglabs(username, accountUsernames.password);
  if (username.includes('locked_out_user')){
    const getErrorText = await loginPage.getErrorText();
    expect(getErrorText).toContain(errorMessage.lockedOut);
} else {
    await expect.soft(page).toHaveURL(inventoryUrl);
    await loginPage.logOut();
    await expect.soft(page).toHaveURL(loginUrl)
}
});
}

// Verify that the user should not be able to access the app when wrong username and password is provided
for (const wrongusername of wrongCredentials.wrongUsernames) {
  for (const wrongpassword of wrongCredentials.wrongPasswords) {
    const expectedMessage = errorMessage[`${wrongusername}-${wrongpassword}`];
  test(`Login with wrong credentials : ${wrongusername} and ${wrongpassword}`, async ({ page }) => {
    const loginPage = new LoginPage(page); // Instantiate the LoginPage class
    await loginPage.signInSwaglabs(wrongusername, wrongpassword);
    const getErrorText = await loginPage.getErrorText();
    expect(getErrorText).toContain(expectedMessage);
    
  });
  }
}
